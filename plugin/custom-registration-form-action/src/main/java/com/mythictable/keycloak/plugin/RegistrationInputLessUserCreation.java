package com.mythictable.keycloak.plugin;

import org.keycloak.Config;
import org.keycloak.authentication.FormAction;
import org.keycloak.authentication.FormActionFactory;
import org.keycloak.authentication.FormContext;
import org.keycloak.authentication.ValidationContext;
import org.keycloak.authentication.forms.RegistrationPage;
import org.keycloak.events.Details;
import org.keycloak.events.Errors;
import org.keycloak.events.EventType;
import org.keycloak.forms.login.LoginFormsProvider;
import org.keycloak.models.AuthenticationExecutionModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.models.utils.FormMessage;
import org.keycloak.protocol.oidc.OIDCLoginProtocol;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.services.messages.Messages;
import org.keycloak.services.resources.AttributeFormDataProcessor;
import org.keycloak.common.util.RandomString;
import org.keycloak.services.validation.Validation;

import javax.ws.rs.core.MultivaluedMap;
import java.util.ArrayList;
import java.util.List;

public class RegistrationInputLessUserCreation implements FormAction, FormActionFactory {
    public static final String PROVIDER_ID = "registration-noinput-user-creation";
    public static final String PREFERRED_USERNAME = "preferredUsername";
    public static final String DISCRIMINATOR = "discriminator";
    public static final int DISCRIMINATOR_LENGTH = 6;
    public static final int NUMBER_OF_ADJECTIVES = 2;

    @Override
    public String getHelpText() {
        return "This action must always be first! Validates and deals with username of the user in validation phase. In success phase, this will create the user in the database.";
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        return null;
    }

    @Override
    public void validate(ValidationContext context) {
        MultivaluedMap<String, String> formData = context.getHttpRequest().getDecodedFormParameters();
        String guest = formData.getFirst("guest");
        if(Validation.isBlank(guest)) {
            
            List<FormMessage> errors = new ArrayList<>();
            context.getEvent().detail(Details.REGISTER_METHOD, "form");

            String email = formData.getFirst(Validation.FIELD_EMAIL);
            context.getEvent().detail(Details.EMAIL, email);
            if (Validation.isBlank(email)) {
                errors.add(new FormMessage(RegistrationPage.FIELD_EMAIL, Messages.MISSING_EMAIL));
            } else if (!Validation.isEmailValid(email)) {
                errors.add(new FormMessage(RegistrationPage.FIELD_EMAIL, Messages.INVALID_EMAIL));
                formData.remove(Validation.FIELD_EMAIL);
            }
            if (errors.size() > 0) {
                context.error(Errors.INVALID_REGISTRATION);
                context.validationError(formData, errors);
                return;
            }
            if (email != null && !context.getRealm().isDuplicateEmailsAllowed() && context.getSession().users().getUserByEmail(email, context.getRealm()) != null) {
                context.error(Errors.EMAIL_IN_USE);
                formData.remove(Validation.FIELD_EMAIL);
                errors.add(new FormMessage(RegistrationPage.FIELD_EMAIL, Messages.EMAIL_EXISTS));
                context.validationError(formData, errors);
                return;
            }
        }
        context.success();
    }

    @Override
    public void buildPage(FormContext context, LoginFormsProvider form) {
        MultivaluedMap<String,String> UrlParameters = context.getUriInfo().getQueryParameters();
        String guest = UrlParameters.getFirst("guest");
        if (!Validation.isBlank(guest)) {
            form.setAttribute("guest", "true");
        }
    }

    @Override
    public void success(FormContext context) {
        MultivaluedMap<String, String> formData = context.getHttpRequest().getDecodedFormParameters();
        String email = formData.getFirst(RegistrationPage.FIELD_EMAIL);
        String preferredUsername;
        if(Validation.isBlank(email))
        {
            preferredUsername = AdjectiveAnimalName.generate(NUMBER_OF_ADJECTIVES);
        } else {
            preferredUsername = email.split("@")[0];
        }

        String discriminator = RandomString.randomCode(DISCRIMINATOR_LENGTH);
        String username = preferredUsername.concat("#")
                .concat(discriminator);
        
        context.getEvent().detail(Details.USERNAME, username)
                .detail(Details.REGISTER_METHOD, "form");
        if(Validation.isBlank(email)) {
            context.getEvent().detail(Details.EMAIL, email);
        }

        UserModel user = context.getSession().users().addUser(context.getRealm(), username);
        user.setEnabled(true);
        user.setSingleAttribute(PREFERRED_USERNAME, preferredUsername);
        user.setSingleAttribute(DISCRIMINATOR, discriminator);
        if(!Validation.isBlank(email)) {
            user.setEmail(email);
        }
        context.getAuthenticationSession().setClientNote(OIDCLoginProtocol.LOGIN_HINT_PARAM, username);
        AttributeFormDataProcessor.process(formData);
        context.setUser(user);
        context.getEvent().user(user);
        context.getEvent().success();
        context.newEvent().event(EventType.LOGIN);
        context.getEvent().client(context.getAuthenticationSession().getClient().getClientId())
                .detail(Details.REDIRECT_URI, context.getAuthenticationSession().getRedirectUri())
                .detail(Details.AUTH_METHOD, context.getAuthenticationSession().getProtocol());
        String authType = context.getAuthenticationSession().getAuthNote(Details.AUTH_TYPE);
        if (authType != null) {
            context.getEvent().detail(Details.AUTH_TYPE, authType);
        }
    }

    @Override
    public boolean requiresUser() {
        return false;
    }

    @Override
    public boolean configuredFor(KeycloakSession session, RealmModel realm, UserModel user) {
        return true;
    }

    @Override
    public void setRequiredActions(KeycloakSession session, RealmModel realm, UserModel user) {

    }

    @Override
    public boolean isUserSetupAllowed() {
        return false;
    }


    @Override
    public void close() {

    }

    @Override
    public String getDisplayType() {
        return "Registration Input-less User Creation";
    }

    @Override
    public String getReferenceCategory() {
        return null;
    }

    @Override
    public boolean isConfigurable() {
        return false;
    }

    private static AuthenticationExecutionModel.Requirement[] REQUIREMENT_CHOICES = {
            AuthenticationExecutionModel.Requirement.REQUIRED,
            AuthenticationExecutionModel.Requirement.DISABLED
    };
    @Override
    public AuthenticationExecutionModel.Requirement[] getRequirementChoices() {
        return REQUIREMENT_CHOICES;
    }
    @Override
    public FormAction create(KeycloakSession session) {
        return this;
    }

    @Override
    public void init(Config.Scope config) {

    }

    @Override
    public void postInit(KeycloakSessionFactory factory) {

    }

    @Override
    public String getId() {
        return PROVIDER_ID;
    }
}
