resource "keycloak_generic_protocol_mapper" "local_group_mapper" {
  realm_id  = keycloak_realm.MythicTable.id
  client_id = keycloak_openid_client.local.id

  name            = "Show Groups"
  protocol        = "openid-connect"
  protocol_mapper = "oidc-group-membership-mapper"

  config = {
    "access.token.claim" : "true",
    "claim.name" : "groups",
    "full.path" : "true",
    "id.token.claim" : "true",
    "userinfo.token.claim" : "true"
  }
}

resource "keycloak_generic_protocol_mapper" "fp_group_mapper" {
  realm_id  = keycloak_realm.MythicTable.id
  client_id = keycloak_openid_client.fp.id

  name            = "Show Groups"
  protocol        = "openid-connect"
  protocol_mapper = "oidc-group-membership-mapper"

  config = {
    "access.token.claim" : "true",
    "claim.name" : "groups",
    "full.path" : "true",
    "id.token.claim" : "true",
    "userinfo.token.claim" : "true"
  }
}

resource "keycloak_generic_protocol_mapper" "edge_group_mapper" {
  realm_id  = keycloak_realm.MythicTable.id
  client_id = keycloak_openid_client.edge.id

  name            = "Show Groups"
  protocol        = "openid-connect"
  protocol_mapper = "oidc-group-membership-mapper"

  config = {
    "access.token.claim" : "true",
    "claim.name" : "groups",
    "full.path" : "true",
    "id.token.claim" : "true",
    "userinfo.token.claim" : "true"
  }
}