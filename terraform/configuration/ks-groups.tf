resource "keycloak_group" "bronze_ks" {
  realm_id = keycloak_realm.MythicTable.id
  name     = "Bronze Kickstarter"
}

resource "keycloak_group" "silver_ks" {
  realm_id = keycloak_realm.MythicTable.id
  name     = "Silver Kickstarter"
}

resource "keycloak_group" "gold_ks" {
  realm_id = keycloak_realm.MythicTable.id
  name     = "Gold Kickstarter"
}

resource "keycloak_group" "plat_ks" {
  realm_id = keycloak_realm.MythicTable.id
  name     = "Platinum Kickstarter"
}