resource "keycloak_openid_client" "fp" {
  client_id   = "MythicTableFirstPlayable"
  enabled     = true
  realm_id    = keycloak_realm.MythicTable.id
  access_type = "PUBLIC"
  admin_url   = "https://fp.mythictable.com"
  root_url    = "https://fp.mythictable.com"
  valid_redirect_uris = [
    "https://fp.mythictable.com/*"
  ]
  web_origins = [
    "https://fp.mythictable.com"
  ]

  backchannel_logout_session_required = false
  direct_access_grants_enabled        = true
  full_scope_allowed                  = true
  standard_flow_enabled               = true
  use_refresh_tokens                  = false
}

resource "keycloak_openid_client" "edge" {
  client_id   = "MythicTableVueFrontEnd"
  enabled     = true
  realm_id    = keycloak_realm.MythicTable.id
  access_type = "PUBLIC"
  admin_url   = "https://edge.mythictable.com"
  root_url    = "https://edge.mythictable.com"
  valid_redirect_uris = [
    "https://edge.mythictable.com/*"
  ]
  web_origins = [
    "https://edge.mythictable.com"
  ]

  backchannel_logout_session_required = false
  direct_access_grants_enabled        = true
  service_accounts_enabled            = false
  standard_flow_enabled               = true
  use_refresh_tokens                  = false
}

resource "keycloak_openid_client" "local" {
  client_id   = "MythicTableLocal"
  realm_id    = keycloak_realm.MythicTable.id
  enabled     = true
  access_type = "PUBLIC"
  admin_url   = "http://localhost:5000"
  root_url    = "http://localhost:5000"
  valid_redirect_uris = [
    "http://localhost:5000/*"
  ]
  web_origins = [
    "http://localhost:5000"
  ]

  backchannel_logout_session_required = false
  direct_access_grants_enabled        = true
  standard_flow_enabled               = true
  use_refresh_tokens                  = false
}